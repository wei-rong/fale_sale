package cn.wolfcode.web.controller;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.OperateIntegralVo;
import cn.wolfcode.service.IUsableIntegralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lanxw
 */
@RestController
@RequestMapping("/integral")
public class IntegralController {
    @Autowired
    private IUsableIntegralService usableIntegralService;
    @RequestMapping("/pay")
    public Result<Boolean> pay(@RequestBody OperateIntegralVo vo){
        int effectCount = usableIntegralService.decrIntegralTry(vo,null);
        return Result.success(effectCount>0);
    }
    @RequestMapping("/refund")
    public Result<Boolean> refund(@RequestBody OperateIntegralVo vo){
        usableIntegralService.addIntegral(vo);
        return Result.success();
    }
}
