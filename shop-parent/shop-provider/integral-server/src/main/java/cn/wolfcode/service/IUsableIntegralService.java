package cn.wolfcode.service;

import cn.wolfcode.domain.OperateIntegralVo;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

/**
 * Created by lanxw
 */
@LocalTCC
public interface IUsableIntegralService {
    /**
     * 积分扣减逻辑
     * @param vo
     */
    int decrIntegral(OperateIntegralVo vo);

    /**
     * 积分增加逻辑
     * @param vo
     */
    void addIntegral(OperateIntegralVo vo);
    @TwoPhaseBusinessAction(name = "decrIntegralTry",commitMethod = "decrIntegralCommit",rollbackMethod ="decrIntegralRollback")
    int decrIntegralTry(@BusinessActionContextParameter(paramName = "vo")OperateIntegralVo vo, BusinessActionContext context);
    void decrIntegralCommit(BusinessActionContext context);
    void decrIntegralRollback(BusinessActionContext context);
}
