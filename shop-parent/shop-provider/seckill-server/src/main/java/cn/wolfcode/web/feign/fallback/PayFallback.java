package cn.wolfcode.web.feign.fallback;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.PayVo;
import cn.wolfcode.domain.RefundVo;
import cn.wolfcode.web.feign.PayFeignApi;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by wolfcode-lanxw
 */
@Component
public class PayFallback implements PayFeignApi {
    @Override
    public Result<String> pay(PayVo payVo) {
        return null;
    }

    @Override
    public Result<Boolean> rsaCheckV1(Map<String, String> params) {
        return null;
    }

    @Override
    public Result<Boolean> refund(RefundVo vo) {
        return null;
    }
}
