package cn.wolfcode.service;

import cn.wolfcode.domain.SeckillProduct;
import cn.wolfcode.domain.SeckillProductVo;

import java.util.List;

/**
 * Created by lanxw
 */
public interface ISeckillProductService {
    /**
     * 根据场次查询秒杀商品集合
     * @param time
     * @return
     */
    List<SeckillProductVo> queryByTime(Integer time);

    /**
     * 根据场次ID和秒杀ID查询秒杀商品数据
     * @param seckillId
     * @param time
     * @return
     */
    SeckillProductVo find(Long seckillId, Integer time);

    /**
     * 库存的扣减
     * @param seckillId
     */
    int decrStockCount(Long seckillId);

    /**
     * 从Redis获取商品集合
     * @param time
     * @return
     */
    List<SeckillProductVo> queryByTimeFromCache(Integer time);

    /**
     * 从Redis获取秒杀商品的详情信息
     * @param seckillId
     * @param time
     * @return
     */
    SeckillProductVo findFromCache(Long seckillId, Integer time);

    /**
     * 进行预库存回补
     * @param time
     * @param seckillId
     */
    void syncStockCountToRedis(Integer time, Long seckillId);

    /**
     * 指定的秒杀商品增加库存
     * @param seckillId
     */
    void incrStockCount(Long seckillId);
}
