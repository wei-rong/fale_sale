package cn.wolfcode.web.controller;

import cn.wolfcode.common.constants.CommonConstants;
import cn.wolfcode.common.web.CommonCodeMsg;
import cn.wolfcode.common.web.Result;
import cn.wolfcode.common.web.anno.RequireLogin;
import cn.wolfcode.domain.OrderInfo;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.mq.MQConstant;
import cn.wolfcode.mq.OrderMessage;
import cn.wolfcode.redis.SeckillRedisKey;
import cn.wolfcode.service.IOrderInfoService;
import cn.wolfcode.service.ISeckillProductService;
import cn.wolfcode.util.DateUtil;
import cn.wolfcode.util.UserUtil;
import cn.wolfcode.web.msg.SeckillCodeMsg;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by lanxw
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderInfoController {
    @Autowired
    private ISeckillProductService seckillProductService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private IOrderInfoService orderInfoService;

    /**
     * 200线程10次请求
     * 896qps
     * 200线程100次请求
     * 6500qps
     * @param seckillId
     * @param time
     * @param request
     * @return
     */
    @RequestMapping("/doSeckill")
    @RequireLogin
    public Result<String> doSeckill(Long seckillId, Integer time, HttpServletRequest request){
        SeckillProductVo vo = seckillProductService.findFromCache(seckillId, time);
        //1.判断日期是否合法
        /*if(!DateUtil.isLegalTime(vo.getStartDate(),vo.getTime())){
            return Result.error(CommonCodeMsg.ILLEGAL_OPERATION);
        }*/
        //2.判断是否重复下单 (查询订单表中是否有该记录)
        String token = request.getHeader(CommonConstants.TOKEN_NAME);
        String phone = UserUtil.getUserPhone(redisTemplate, token);
        String orderSetKey = SeckillRedisKey.SECKILL_ORDER_SET.getRealKey(String.valueOf(seckillId));
        if(redisTemplate.opsForSet().isMember(orderSetKey,phone)){
            //说明数据库存在这样的记录，说明当前用户已经抢到商品了，不应该重复抢购
            return Result.error(SeckillCodeMsg.REPEAT_SECKILL);
        }
        //3.Redis原子性递减，控制进行Service的请求人数
        String stockCountKey = SeckillRedisKey.SECKILL_STOCK_COUNT_HASH.getRealKey(String.valueOf(time));
        long remainCount = redisTemplate.opsForHash().increment(stockCountKey,String.valueOf(seckillId),-1);
        if(remainCount<0){
            return Result.error(SeckillCodeMsg.SECKILL_STOCK_OVER);
        }
        //变成MQ异步下单
        OrderMessage msg = new OrderMessage(time,seckillId,token,Long.parseLong(phone));
        rocketMQTemplate.syncSend(MQConstant.ORDER_PEDDING_TOPIC,msg);

        return Result.success("成功进入队列，请耐心等待结果");
    }
    @RequestMapping("/find")
    @RequireLogin
    public Result<OrderInfo> find(String orderNo,HttpServletRequest request){
        OrderInfo orderInfo = orderInfoService.find(orderNo);
        String token = request.getHeader(CommonConstants.TOKEN_NAME);
        String phone = UserUtil.getUserPhone(redisTemplate, token);
        //只能查看自己的订单
        if(!phone.equals(String.valueOf(orderInfo.getUserId()))){
           return Result.error(CommonCodeMsg.ILLEGAL_OPERATION);
        }
        return Result.success(orderInfo);
    }
}
