package cn.wolfcode.mq;

import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.service.IOrderInfoService;
import cn.wolfcode.service.ISeckillProductService;
import cn.wolfcode.web.msg.SeckillCodeMsg;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by wolfcode-lanxw
 */
@Component
@RocketMQMessageListener(consumerGroup = "orderPeddingGroup",topic = MQConstant.ORDER_PEDDING_TOPIC)
public class OrderPeddingListener implements RocketMQListener<OrderMessage> {
    @Autowired
    private IOrderInfoService orderInfoService;
    @Autowired
    private ISeckillProductService seckillProductService;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Override
    public void onMessage(OrderMessage message) {
        System.out.println("执行秒杀的逻辑");
        OrderMQResult result = new OrderMQResult();
        result.setToken(message.getToken());
        String tag = ":";
        try{
            SeckillProductVo vo = seckillProductService.findFromCache(message.getSeckillId(),message.getTime());
            String orderNo = orderInfoService.createOrder(vo, String.valueOf(message.getUserPhone()));
            //业务成功
            result.setOrderNo(orderNo);
            tag+=MQConstant.ORDER_RESULT_SUCCESS_TAG;
            //发送延时消息
            Message msg = MessageBuilder.withPayload(result).build();
            rocketMQTemplate.syncSend(MQConstant.ORDER_PAY_TIMEOUT_TOPIC,msg,3000,MQConstant.ORDER_PAY_TIMEOUT_DELAY_LEVEL);
        }catch(Exception e){
            e.printStackTrace();
            //业务失败
            result.setTime(message.getTime());
            result.setSeckillId(message.getSeckillId());
            result.setCode(SeckillCodeMsg.SECKILL_ERROR.getCode());
            result.setMsg(SeckillCodeMsg.SECKILL_ERROR.getMsg());
            tag+=MQConstant.ORDER_RESULT_FAIL_TAG;
        }
        System.out.println("发送结果");
        rocketMQTemplate.syncSend(MQConstant.ORDER_RESULT_TOPIC+tag,result);
    }
}
