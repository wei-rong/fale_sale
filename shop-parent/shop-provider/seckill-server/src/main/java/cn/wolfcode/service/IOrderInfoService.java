package cn.wolfcode.service;


import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.OrderInfo;
import cn.wolfcode.domain.SeckillProductVo;

import java.util.Map;

/**
 * Created by wolfcode-lanxw
 */
public interface IOrderInfoService {
    /**
     * 根据手机号码和秒杀ID查询订单西信息
     * @param seckillId
     * @param phone
     * @return
     */
    OrderInfo findByUserIdAndSeckillId(Long seckillId, String phone);

    /**
     * 进行订单创建
     * @param vo
     * @param phone
     * @return
     */
    String createOrder(SeckillProductVo vo, String phone);

    /**
     * 根据订单号查询订单对象
     * @param orderNo
     * @return
     */
    OrderInfo find(String orderNo);

    /**
     * 取消超时支付的订单
     * @param orderNo
     */
    void cancelPayTimeOutOrder(String orderNo);

    /**
     * 调用支付服务获取form表单字符串
     * @param orderNO
     * @return
     */
    Result<String> payOnLine(String orderNO);

    /**
     * 支付成功的回调
     * @param orderNo
     */
    void paySuccess(String orderNo);

    /**
     * 进行在线退款
     * @param orderInfo
     */
    void refundOnline(OrderInfo orderInfo);

    /**
     * 积分支付逻辑
     * @param orderNo
     */
    void payByIntegral(String orderNo);

    void refundByIntegral(OrderInfo orderInfo);
}
