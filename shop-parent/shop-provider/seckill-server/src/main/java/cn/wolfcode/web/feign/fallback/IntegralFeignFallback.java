package cn.wolfcode.web.feign.fallback;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.OperateIntegralVo;
import cn.wolfcode.web.feign.IntegralFeignApi;
import org.springframework.stereotype.Component;

/**
 * Created by wolfcode-lanxw
 */
@Component
public class IntegralFeignFallback implements IntegralFeignApi {
    @Override
    public Result pay(OperateIntegralVo vo) {
        return null;
    }

    @Override
    public Result<Boolean> refund(OperateIntegralVo vo) {
        return null;
    }
}
