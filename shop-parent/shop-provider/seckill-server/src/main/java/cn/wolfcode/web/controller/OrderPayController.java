package cn.wolfcode.web.controller;


import cn.wolfcode.common.constants.CommonConstants;
import cn.wolfcode.common.web.CommonCodeMsg;
import cn.wolfcode.common.web.Result;
import cn.wolfcode.common.web.anno.RequireLogin;
import cn.wolfcode.domain.OrderInfo;
import cn.wolfcode.service.IOrderInfoService;
import cn.wolfcode.util.UserUtil;
import cn.wolfcode.web.feign.PayFeignApi;
import cn.wolfcode.web.msg.SeckillCodeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Created by lanxw
 */
@RestController
@RequestMapping("/orderPay")
@RefreshScope
public class OrderPayController {
    @Autowired
    private IOrderInfoService orderInfoService;
    @Autowired
    private PayFeignApi payFeignApi;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @RequestMapping("/pay")
    public Result<String> pay(Integer type,String orderNo){
        if(OrderInfo.PAYTYPE_ONLINE.equals(type)){
            //在线支付
            return orderInfoService.payOnLine(orderNo);
        }else{
            //积分支付
            orderInfoService.payByIntegral(orderNo);
            return Result.success();
        }
    }
    //http://ap6gum.natappfree.cc/seckill-service/orderPay/notifyUrl
    @RequestMapping("/notifyUrl")
    public String notifyUrl(@RequestParam Map<String,String> params){
        System.out.println("异步回调"+new Date());
        Result<Boolean> result = payFeignApi.rsaCheckV1(params);
        if(result==null || result.hasError()){
            return "fail";
        }
        boolean signVerified = result.getData();
        if(signVerified){
            //业务逻辑
            orderInfoService.paySuccess(params.get("out_trade_no"));
            return "success";
        }else{
            return "fail";
        }
    }
    @Value("${pay.errorUrl}")
    private String errorUrl;
    @Value("${pay.frontEndPayUrl}")
    private String frontEndPayUrl;
    //http://ap6gum.natappfree.cc/seckill-service/orderPay/returnUrl
    @RequestMapping("/returnUrl")
    public void returnUrl(@RequestParam Map<String,String> params,HttpServletResponse response) throws IOException {
        System.out.println("同步回调"+new Date());
        Result<Boolean> result = payFeignApi.rsaCheckV1(params);
        if(result==null || result.hasError()){
            response.sendRedirect(errorUrl);
            return;
        }
        String orderNo = params.get("out_trade_no");
        //跳转订单详情页面
        response.sendRedirect(frontEndPayUrl+orderNo);
    }

    @RequestMapping("/refund")
    @RequireLogin
    public Result refund(String orderNo, HttpServletRequest request){
        OrderInfo orderInfo = orderInfoService.find(orderNo);
        String token = request.getHeader(CommonConstants.TOKEN_NAME);
        String phone = UserUtil.getUserPhone(redisTemplate, token);
        //只能对自己的订单进行退款
        if(!phone.equals(String.valueOf(orderInfo.getUserId()))){
            return Result.error(CommonCodeMsg.ILLEGAL_OPERATION);
        }
        if(OrderInfo.STATUS_ACCOUNT_PAID.equals(orderInfo.getStatus())){
            if(OrderInfo.PAYTYPE_ONLINE.equals(orderInfo.getPayType())){
                //在线支付
                orderInfoService.refundOnline(orderInfo);
            }else{
                //积分支付
                orderInfoService.refundByIntegral(orderInfo);
            }
        }
        return Result.success();
    }
}
