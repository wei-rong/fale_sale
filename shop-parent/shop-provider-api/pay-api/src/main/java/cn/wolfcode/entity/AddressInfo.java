package cn.wolfcode.entity;

import lombok.Data;

/**
 * @author: weiRon
 * @description 地址信息
 * @date: 2023/2/28 14:43
 */
@Data
public class AddressInfo {
    private Integer id;
    private String name;
    private String address;
}
