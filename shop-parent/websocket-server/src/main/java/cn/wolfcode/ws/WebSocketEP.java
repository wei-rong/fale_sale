package cn.wolfcode.ws;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by wolfcode-lanxw
 */
@Component
@ServerEndpoint("/{token}")
public class WebSocketEP {
    public static ConcurrentHashMap<String,Session> clients = new ConcurrentHashMap<String,Session>();
    @OnOpen
    public void onOpen(@PathParam("token") String token, Session session){
        System.out.println("客户端和服务器建立会话,"+token);
        clients.put(token,session);
    }
    @OnClose
    public void onClose(@PathParam("token") String token){
        clients.remove(token);
    }
    @OnError
    public void onError(Throwable error){
        error.printStackTrace();
    }
}
